const client = require('./twilio-client');

function isRoomInProgress(roomName) {
  return new Promise((resolve, reject) => {
    client.video.rooms
      .list({uniqueName: roomName, status: 'in-progress', limit: 20})
      .then(rooms => resolve(rooms.length > 0 ? rooms[0] : false))
      .catch(e => reject(e));
  });
}

module.exports = { isRoomInProgress };