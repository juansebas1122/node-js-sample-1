const https = require('https')
const cors = require('cors');
const express = require('express');
const fs = require('fs');
const app = express();
app.use(cors({ 
  origin: [
    'http://localhost:', 
    'https://localhost:', 
    'http://localhost:3002', 'http://192.168.1.76:3002',
    'https://localhost:3003', 'https://192.168.1.76:3003',
    'https://desktop-lgticns:3003'
  ] 
}));
/* app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Headers", 'token-auth');
  // res.setHeader("Access-Control-Expose-Headers", 'room-info');
  next();
});*/
const port = process.env.PORT || 3000;
/*  Testing https
app.get('/', (req, res) => res.send({"msg": "Hello twilio from node.js"}));
https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
}, app).listen(port, () => console.log("Listenen...", port));*/
app.listen(port, () => console.log("Listenen...", port));
app.get('/test', (req, res) => res.send({"msg": "testing..."}));
module.exports = app;