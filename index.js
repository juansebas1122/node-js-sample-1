require('dotenv').config();

const client = require('./twilio-client');
const tokenGenerator = require('./token');
const app = require('./app');
const twilioUtils = require('./twilio-utils');

app.get('/rooms/verify/:roomName', (req, res) => {
  twilioUtils.isRoomInProgress(req.params.roomName)
    .then(result => res.send(result))
    .catch(e => res.status(500).send(e));
});

app.get('/room/initialize/:roomName/:participant', async (req, res) => {  
  // accountSid -> Your Account SID from www.twilio.com/console
  // authToken  -> Your Auth Token from www.twilio.com/console
  const roomName = req.params.roomName;
  let isRoomInProgress = await twilioUtils.isRoomInProgress(req.params.roomName);
  let room;
  if (!isRoomInProgress) { // Create room when isn't is progress
    room = await client.video.rooms.create({
      type: 'peer-to-peer',
      uniqueName: roomName
    });
    console.info("Room created...");
    console.log(room);
  }  
  console.info(`Generating token to connect ${req.params.participant}...`);
  let token = tokenGenerator(req.params.participant, roomName);
  res.set('token-auth', token);
  if (room) res.set('room-info', JSON.stringify(room));
  // res.send(true);
  res.send(token);
});